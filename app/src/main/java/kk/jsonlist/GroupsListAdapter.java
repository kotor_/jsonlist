package kk.jsonlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kk.jsonlist.model.Group;

public class GroupsListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private final ArrayList<Group> groups;

    public GroupsListAdapter(Context context, ArrayList<Group> items) {
        this.mContext = context;
        this.groups = items;
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).getItems().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).getItems().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groups.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groups.get(groupPosition).getItems().get(childPosition).getId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final GroupsViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.group_list_element, parent, false);

            viewHolder = new GroupsViewHolder();
            viewHolder.tvGroupName = (TextView) convertView.findViewById(R.id.tv_group_name);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (GroupsViewHolder) convertView.getTag();
        }

        viewHolder.tvGroupName.setText(groups.get(groupPosition).getName());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ItemsViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list_element, parent, false);

            viewHolder = new ItemsViewHolder();
            viewHolder.tvItemName = (TextView) convertView.findViewById(R.id.tv_item_name);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ItemsViewHolder) convertView.getTag();
        }

        viewHolder.tvItemName.setText(groups.get(groupPosition).getItems().get(childPosition).getName());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class GroupsViewHolder {
        public TextView tvGroupName;
    }

    private class ItemsViewHolder {
        public TextView tvItemName;
    }
}
