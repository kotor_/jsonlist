package kk.jsonlist;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import kk.jsonlist.model.Group;
import kk.jsonlist.model.Item;

public class MainFragment extends Fragment {
    public static final String JSON_FILE = "data.json";

    private View mainView;
    private ExpandableListView elvGroupsList;
    private ArrayList<Group> groupsList;

    public MainFragment() {
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mainView = getView();

        groupsList = new ArrayList<>();
        final ArrayList<Group> notEmptyGroupsList = new ArrayList<>();

        if (mainView != null) {
            elvGroupsList = (ExpandableListView) mainView.findViewById(R.id.elv_groups_list);
        }
        GroupsListAdapter glaGroupsAdapter = new GroupsListAdapter(getActivity(), notEmptyGroupsList);
        if (elvGroupsList != null) {
            elvGroupsList.setAdapter(glaGroupsAdapter);

            elvGroupsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    long pos = elvGroupsList.getExpandableListPosition(position);

                    int itemType = ExpandableListView.getPackedPositionType(pos);
                    int groupPosition = ExpandableListView.getPackedPositionGroup(pos);
                    int childPosition = ExpandableListView.getPackedPositionChild(pos);


                    Group group = notEmptyGroupsList.get(groupPosition);

                    if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                        Animation animation;
                        if (group.getId() % 2 == 0) {
                            animation = new RotateAnimation(360.0f, 0.0f, mainView.getWidth() / 2, mainView.getHeight() / 2);
                        } else {
                            animation = new RotateAnimation(0.0f, 360.0f, mainView.getWidth() / 2, mainView.getHeight() / 2);

                        }
                        animation.setDuration(1000);
                        elvGroupsList.setAnimation(animation);
                    } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                        new AlertDialog.Builder(getActivity())
                                .setTitle(group.getName())
                                .setMessage(group.getItems().get(childPosition).getName())
                                .setPositiveButton(getActivity().getResources().getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .show();
                    }
                    return false;
                }
            });
        }

        try {
            parseJson(loadJSONFromAsset(JSON_FILE));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (Group g : groupsList) {
            if (!g.getItems().isEmpty()) {
                notEmptyGroupsList.add(g);
            }
        }
        glaGroupsAdapter.notifyDataSetChanged();
    }

    private String loadJSONFromAsset(String file) {
        String json;
        try {
            InputStream is = getActivity().getAssets().open(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void parseJson(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);

        if (obj.has("groups")) {
            JSONArray groups = obj.getJSONArray("groups");

            for (int i = 0; i < groups.length(); i++) {
                JSONObject group = groups.getJSONObject(i);
                if (group.has("id") && group.has("name")) {
                    groupsList.add(new Group(group.getInt("id"), group.getString("name")));
                }
            }
        }

        if (obj.has("items")) {
            JSONArray items = obj.getJSONArray("items");

            for (int i = 0; i < items.length(); i++) {
                JSONObject item = items.getJSONObject(i);
                if (item.has("id") && item.has("group_id") && item.has("name")) {
                    addItemToGroup(new Item(item.getInt("id"), item.getInt("group_id"), item.getString("name")));
                }
            }
        }
    }

    private void addItemToGroup(Item item) {
        for (Group g : groupsList) {
            if (g.getId().equals(item.getGroupId())) {
                g.addItem(item);
            }
        }
    }
}
