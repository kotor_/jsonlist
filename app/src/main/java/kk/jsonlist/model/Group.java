package kk.jsonlist.model;

import java.util.ArrayList;

public class Group {
    private Integer id;
    private String name;
    private ArrayList<Item> items;

    public Group(Integer id, String name) {
        this.id = id;
        this.name = name;
        this.items = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {
        items.add(item);
    }
}
