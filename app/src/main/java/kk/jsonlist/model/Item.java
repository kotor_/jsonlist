package kk.jsonlist.model;

public class Item {
    private Integer id;
    private Integer group_id;
    private String name;

    public Item(Integer id, Integer group_id, String name) {
        this.id = id;
        this.group_id = group_id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public Integer getGroupId() {
        return group_id;
    }

    public String getName() {
        return name;
    }
}
