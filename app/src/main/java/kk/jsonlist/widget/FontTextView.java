package kk.jsonlist.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontTextView extends TextView {
    private static Typeface font;

    public FontTextView(Context context) {
        super(context);
        setFont(context);
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    private void setFont(Context context) {
        if (this.isInEditMode()) {
            return;
        }

        if (font == null) {
            font = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Light.ttf");
        }

        setTypeface(font);
    }
}
